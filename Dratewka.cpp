//
// Created by kuba on 12.05.17.
//
#include <iostream>
#include <sstream>
#include "Dratewka.h"

Dratewka::Dratewka(int id) : ThreadSafe(){
    this->id = id;
    this->state = AT_THE_CITY;
    this->seed = rand();
}

int Dratewka::getId() {
    return this->id;
}

void Dratewka::setId(int id) {
    this->id = id;
}

uint * Dratewka::getSeed() {
	return &seed;
}

DratewkaState Dratewka::getState() {
    return this->state;
}

void Dratewka::setState(DratewkaState state) {
    this->state = state;
}

string Dratewka::getPrintable() {
    ostringstream stream;
    stream << "Id: " << this->id
    		<< " | Stan: " << dratewkaStateToString(this->getState())
    		<< " | Postep: ";
    for(auto i = 0; i < this->getProgress(); i += 10) {
        stream << "#";
    }
    return stream.str();
}


