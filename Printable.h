//
// Created by kuba on 03.06.17.
//

#ifndef DRATEWKATHEANNIHILATOR_PRINTABLE_H
#define DRATEWKATHEANNIHILATOR_PRINTABLE_H

#include <string>
using namespace std;

class Printable {
public:
    virtual string getPrintable() = 0;
};


#endif //DRATEWKATHEANNIHILATOR_PRINTABLE_H
