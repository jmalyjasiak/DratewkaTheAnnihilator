#include <pthread.h>
#include <curses.h>
#include <iostream>
#include <unistd.h>
#include <time.h>
#include "Cave.h"
#include "CaveOccupationState.h"
#include "City.h"
#include "Dragon.h"
#include "DragonState.h"
#include "Dratewka.h"
#include "DratewkaState.h"
#include "FinishCondition.h"
using namespace std;

Dratewka ** dratewkas;
Dragon ** dragons;
Cave ** caves;
City * city;

int dratewkasCount;
int dragonsCount;
int cityHp;

//delays in microseconds
int dratewkaAtTheCityDelay = 500000;
int dratewkaLookingForDragonDelay = 2000000;
int dratewkaMovementDelayMin = 1000000;
int dratewkaMovementDelayMax = 3000000;
int owieczkaPlantingDelay = 2000000;
int dratewkaResurrectionDelayMin = 2000000;
int dratewkaResurrectionDelayMax = 4000000;

int dragonMovementDelayMin = 700000;
int dragonMovementDelayMax = 2000000;
int dragonAttackDelayMin = 1000000;
int dragonAttackDelayMax = 3000000;
int dragonInCaveDelayMin = 1000000;
int dragonInCaveDelayMax = 2000000;


//Main thread arrays
pthread_t * dratewkaThreads;
pthread_t * dragonThreads;

pthread_mutex_t paintMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t finishMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t findDragonMutex = PTHREAD_MUTEX_INITIALIZER;


bool repaint = true;
bool finish = false;
FinishCondition finishCondition;

//printing coordinates
uint dratewkasRow;
uint dragonsRow;
uint cavesRow;
uint cityRow;
uint endRow;

void initCoordinates();
void initData();
void initDratewkas();
void initDragons();
void initCaves();
void initThreads();
void * dratewkaRoutine(void * arg);
void * dragonRoutine(void * arg);

void printData();
void printDratewka(int id);
void printDragon(int id);
void printCave(int id);
void printCity();
void printHeaders();
void clearRow(int row);

void tearDown();

Dragon * findRandomDragon();
inline int getRandomDelay(int min, int max);

int main(int argc, char ** argv) {
    if (argc != 4) {
        cout << "Invalid args. Found "<<argc<<", expected 4:\n<number of Dratewkas> <number of dragons> <city's HP>" <<endl;
        return 0;
    }

    dratewkasCount = atoi(argv[1]);
    dragonsCount = atoi(argv[2]);
    cityHp = atoi(argv[3]);

    srand(time(nullptr));
    initscr();

    city = new City(cityHp);
    initData();
    initCoordinates();
    printHeaders();

    initThreads();

    getch();
    tearDown();
    return 0;
}

void initCoordinates() {
	int sectionHeaderRowsCount = 1;
	int sectionFooterRowsCount = 1;

	dratewkasRow = 1;
	dragonsRow = dratewkasRow+sectionHeaderRowsCount + sectionFooterRowsCount + dratewkasCount;
	cavesRow = dragonsRow+sectionHeaderRowsCount + sectionFooterRowsCount +  dragonsCount;
	cityRow = cavesRow+sectionHeaderRowsCount + sectionFooterRowsCount + dragonsCount;
	endRow = cavesRow+sectionHeaderRowsCount + sectionFooterRowsCount + 1;
}

void initData(){
    initDratewkas();
    initDragons();
    initCaves();
}

void initDratewkas() {
    dratewkas = new Dratewka*[dratewkasCount];
    for(auto i = 0; i < dratewkasCount; i++) {
        dratewkas[i] = new Dratewka(i);
    }
}

void initDragons() {
    dragons = new Dragon*[dragonsCount];
    for(size_t i = 0; i < dragonsCount; i++) {
        dragons[i] = new Dragon(i);
    }
}

void initCaves() {
    caves = new Cave*[dragonsCount];
    for(size_t i = 0; i < dragonsCount; i++) {
        caves[i] = new Cave(i);
        dragons[i]->setCave(caves[i]);
    }
}

void * dratewkaRoutine(void * arg) {
    Dratewka * dratewka = (Dratewka *)arg;
    int delay;
    while(true) {
    	Dragon * dragon = nullptr;
    	while(dragon == nullptr) {
			dratewka->setState(DratewkaState::AT_THE_CITY);
			dratewka->reset();
			for(int i = 0; i < 100; i+=10) {
				usleep(dratewkaAtTheCityDelay);
				dratewka->add(10);
				printDratewka(dratewka->getId());
			}
			dratewka->setState(DratewkaState::LOOKING_FOR_DRAGON);
			dratewka->reset();
			int numOfRetry = 3;
			for(auto i = 0; i < numOfRetry; i++) {
				usleep(dratewkaLookingForDragonDelay);
				pthread_mutex_lock(&findDragonMutex);
				dragon = findRandomDragon();
				pthread_mutex_unlock(&findDragonMutex);
				if(dragon != nullptr) {
					break;
				}
				dratewka->add(30);
				printDratewka(dratewka->getId());
			}
    	}

    	Cave * cave = dragon->getCave();
    	dratewka->setState(DratewkaState::CHASING_DRAGON);
    	dratewka->reset();
    	delay = (rand_r(dratewka->getSeed()) * rand_r(dratewka->getSeed())) % (dratewkaMovementDelayMax - dratewkaMovementDelayMin) + dratewkaMovementDelayMin;
    	delay = abs(delay) % 2000000;
    	for(auto i = 0; i < 100; i+=10) {
    		dratewka->add(10);
    		printDratewka(dratewka->getId());
    		usleep(delay);
    	}
    	cave->lock();
    	if(cave->isOccupied()) {
    		cave->unlock();
    		dratewka->setState(DratewkaState::WOUNDED);
    		dratewka->reset();
    		delay =(rand_r(dratewka->getSeed()) * rand_r(dratewka->getSeed())) %  (dratewkaResurrectionDelayMax - dratewkaResurrectionDelayMin) + dratewkaResurrectionDelayMin;
    		delay = abs(delay) % 2000000;
    		for(auto i = 0; i < 100; i+=10) {
    			dratewka->add(10);
    			printDratewka(dratewka->getId());
    			usleep(delay);
    		}
    		continue;
    	}
    	cave->unlock();
    	dratewka->setState(DratewkaState::PLANTING_OWIECZKA);
    	printDratewka(dratewka->getId());
    	usleep(owieczkaPlantingDelay);
    	cave->lock();
    	if(cave->isOccupied()) {
    		cave->unlock();
    		dratewka->setState(DratewkaState::WOUNDED);
    		dratewka->reset();
    		delay = (rand_r(dratewka->getSeed()) * rand_r(dratewka->getSeed())) %  (dratewkaResurrectionDelayMax - dratewkaResurrectionDelayMin) + dratewkaResurrectionDelayMin;
    		delay = abs(delay) % 2000000;
    		for(auto i = 0; i < 100; i+=10) {
    			dratewka->add(10);
    			printDratewka(dratewka->getId());
    			usleep(delay);
    		}
    		continue;
    	}
    	cave->occupy(CaveOccupationState::OWIECZKA_HAS_BEEN_PLANTED);
    	cave->unlock();
    	printCave(cave->getId());
    	dratewka->setState(DratewkaState::RUNNING_TO_CITY);
    	dratewka->reset();
    	delay = (rand_r(dratewka->getSeed()) * rand_r(dratewka->getSeed())) % (dratewkaMovementDelayMax - dratewkaMovementDelayMin) + dratewkaMovementDelayMin;
    	delay = abs(delay) % 2000000;
    	for(auto i = 0; i < 100; i +=10) {
    		dratewka->add(10);
    		printDratewka(dratewka->getId());
    		usleep(delay);
    	}
    }



    pthread_exit(nullptr);
}

Dragon * findRandomDragon() {
	int index;
	bool * checked = new bool[dragonsCount]{false};
	int livingDragons = dragonsCount;
	for(auto i = 0; i < livingDragons; i++) {
		 while (checked[(index= rand() % dragonsCount)]) ;
		 checked[index] = true;
		 dragons[index]->lock();
		 if(dragons[index]->getState() == DragonState::DEAD) {
			 dragons[index]->unlock();
			 continue;
		 }
		 if(dragons[index]->getState() == DragonState::ATTACKING && !dragons[index]->isChased())  {
			 dragons[index]->setChased(true);
			 dragons[index]->unlock();
			 return dragons[index];
		 }
		 dragons[index]->unlock();
	}
	return nullptr;
}

void * dragonRoutine(void * arg) {
    Dragon * dragon = (Dragon *)arg;
    Cave * cave = dragon->getCave();
    int delay;
    while(true) {
		cave->lock();
		cave->unoccupy();
		cave->unlock();
		printCave(cave->getId());
		dragon->lock();
		dragon->setState(DragonState::FLYING_TO_CITY);
		dragon->reset();
		dragon->unlock();
    	delay =(rand_r(dragon->getSeed()) * rand_r(dragon->getSeed())) % (dragonMovementDelayMax - dragonMovementDelayMin) + dragonMovementDelayMin;
    	delay = abs(delay) % 2000000;
		for(auto i = 0; i < 100; i += 10) {
			dragon->add(10);
			printDragon(dragon->getId());
			usleep(100000);
		}
		dragon->lock();
		dragon->setState(DragonState::ATTACKING);
		dragon->unlock();
		printDragon(dragon->getId());
		while(true) {
			delay =(rand_r(dragon->getSeed()) * rand_r(dragon->getSeed())) %  (dragonAttackDelayMax - dragonAttackDelayMin) + dragonAttackDelayMin;
			delay = abs(delay) % 2000000;
			usleep(delay);
			dragon->lock();
			if(dragon->isChased()) {
				dragon->setState(DragonState::FLYING_TO_CAVE);
				dragon->reset();
				dragon->unlock();
		    	delay = (rand_r(dragon->getSeed()) * rand_r(dragon->getSeed())) %(dragonMovementDelayMax - dragonMovementDelayMin) + dragonMovementDelayMin;
		    	delay = abs(delay) % 2000000;
				for(auto i = 0; i < 100; i += 10) {
					dragon->add(10);
					printDragon(dragon->getId());
					usleep(delay);
				}
				cave->lock();
				if(cave->isOccupied()) {
					cave->unlock();
					dragon->lock();
					dragon->setState(DragonState::DEAD);
					dragon->reset();
					dragon->unlock();
					printDragon(dragon->getId());
					break;
				} else {
					cave->unlock();
					dragon->lock();
					dragon->setChased(false);
					dragon->unlock();
					break;
				}
			} else {
				dragon->unlock();
				city->lock();
				bool isDemolished = city->demolish(dragon->attack());
				city->unlock();
				if(isDemolished) {
					pthread_mutex_lock(&finishMutex);
					finish = true;
					finishCondition = FinishCondition::CITY_DEMOLISHED;
					pthread_mutex_unlock(&finishMutex);
					break;
				}
				printCity();
			}
		}
		city->lock();
		if(dragon->isDead() || city->isDemolished()) {
			city->unlock();
			break;
		}
		city->unlock();
		dragon->lock();
		dragon->setState(DragonState::IN_CAVE);
		dragon->reset();
		dragon->unlock();
		cave->lock();
		cave->occupy(CaveOccupationState::BY_DRAGON);
		cave->unlock();
		printCave(dragon->getId());
    	delay =(rand_r(dragon->getSeed()) * rand_r(dragon->getSeed())) % (dragonInCaveDelayMax - dragonInCaveDelayMin) + dragonInCaveDelayMin;
    	delay = abs(delay) % 2000000;
		for(auto i = 0; i < 100; i+= 10) {
			dragon->add(10);
			printDragon(dragon->getId());
			usleep(delay);
		}
    }

    pthread_exit(nullptr);
}

void initThreads() {

    dratewkaThreads = new pthread_t[dratewkasCount];
    for(auto i = 0; i < dratewkasCount; i++) {
        pthread_create(&dratewkaThreads[i], nullptr, dratewkaRoutine, dratewkas[i]);
    }
	dragonThreads = new pthread_t[dragonsCount];
	for(auto i = 0; i < dragonsCount; i++) {
		pthread_create(&dragonThreads[i], nullptr, dragonRoutine, dragons[i]);
	}

}

void printHeaders() {
	int row = dratewkasRow;
	pthread_mutex_lock(&paintMutex);
	mvprintw(row, 0, "---------DRATEWKOWIE-------------------------------------------------");
	for(auto i = 0; i < dratewkasCount; i++) {
		mvprintw(++row, 0, dratewkas[i]->getPrintable().c_str());
	}
	mvprintw(++row, 0, "---------------------------------------------------------------------");
	mvprintw(++row, 0, "---------SMOKI-------------------------------------------------------");
	for(int i = 0; i < dragonsCount; i++) {
		mvprintw(++row, 0, dragons[i]->getPrintable().c_str());
	}
	mvprintw(++row, 0, "---------------------------------------------------------------------");
	mvprintw(++row, 0, "---------JASKINIE----------------------------------------------------");
	for(int i = 0; i<dragonsCount; i++) {
		mvprintw(++row, 0, caves[i]->getPrintable().c_str());
	}
	mvprintw(++row, 0, "---------------------------------------------------------------------");
	mvprintw(++row, 0, "---------MIASTO------------------------------------------------------");
	mvprintw(++row, 0, city->getPrintable().c_str());
	mvprintw(++row, 0, "---------------------------------------------------------------------");
	refresh();
	pthread_mutex_unlock(&paintMutex);
}

void printDratewka(int id){
	pthread_mutex_lock(&paintMutex);
	int rowToPrint = dratewkasRow + id + 1;
	clearRow(rowToPrint);
	move(rowToPrint, 0);
	dratewkas[id]->lock();
	printw("%s", dratewkas[id]->getPrintable().c_str());
	dratewkas[id]->unlock();
	refresh();
	pthread_mutex_unlock(&paintMutex);
}

void printDragon(int id){
	pthread_mutex_lock(&paintMutex);
	int rowToPrint = dragonsRow + id + 1;
	clearRow(rowToPrint);
	move(rowToPrint, 0);
	dragons[id]->lock();
	printw("%s", dragons[id]->getPrintable().c_str());
	dragons[id]->unlock();
	refresh();
	pthread_mutex_unlock(&paintMutex);
}

void printCave(int id) {
	pthread_mutex_lock(&paintMutex);
	int rowToPrint = cavesRow + id + 1;
	clearRow(rowToPrint);
	move(rowToPrint, 0);
	caves[id]->lock();
	printw("%s", caves[id]->getPrintable().c_str());
	caves[id]->unlock();
	refresh();
	pthread_mutex_unlock(&paintMutex);
}

void printCity() {
	pthread_mutex_lock(&paintMutex);
	int rowToPrint = cityRow + 1;
	clearRow(rowToPrint);
	move(rowToPrint, 0);
	city->lock();
	printw("%s", city->getPrintable().c_str());
	city->unlock();
	refresh();
	pthread_mutex_unlock(&paintMutex);
}

void clearRow(int row) {
	int maxRowLength  = 70;
	move(row, 0);
	for(auto i = 0; i < maxRowLength; i++) {
		printw(" ");
	}
}

void tearDown() {
	for(auto i = 0; i < dragonsCount; ++i) {
		delete dragons[i];
		delete caves[i];
		pthread_cancel(dragonThreads[i]);
		pthread_join(dragonThreads[i], nullptr);
	}

	delete [] dragons;
	delete [] caves;

	for(auto i = 0; i < dratewkasCount; i++) {
		delete dratewkas[i];
		pthread_cancel(dratewkaThreads[i]);
		pthread_join(dratewkaThreads[i], nullptr);
	}

	delete [] dratewkas;

	delete city;
    endwin();
}
//TODO Chyba wypisywanie w liniach, pasek postepu i predkosc dla biegnacego dratewki, pasek postepu dla rozgladania sie, pasek postepu dla dolatywania do miasta
//TODO Zaimplementować argumenty, dodać jakieś ograniczenia do argumentów
//TODO Miasto i jaskinia jako zasob. kazdy smok ma swoja jaskinie? Dobre uproszczenie. Dratewka goni smoka do jego jaskini
//TODO nie dziala wypisywanie? Coś jest nie tak
