//
// Created by kuba on 12.05.17.
//

#ifndef DRATEWKATHEANNIHILATOR_DRAGON_H
#define DRATEWKATHEANNIHILATOR_DRAGON_H

#include <pthread.h>
#include <string>
#include "ProgressIndicator.h"
#include "DragonState.h"
#include "Cave.h"
#include "Printable.h"

using namespace std;
class Dragon : public ProgressIndicator, public Printable, public ThreadSafe{

private:
    static const int MAX_DAMAGE = 20;
    int id;
    bool chased;
    DragonState state;
    Cave * cave;
    uint seed;
public:
    Dragon(int id);

    bool isDead();
    static int attack();
    int getId();
    DragonState getState();
    void setState(DragonState state);
    Cave * getCave();
    void setCave(Cave * cave);
    bool isChased();
    void setChased(bool chased);
    uint * getSeed();
    string getPrintable();
    string dragonStateToString(DragonState state);
};
#endif //DRATEWKATHEANNIHILATOR_DRAGON_H
