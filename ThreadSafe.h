//
// Created by kuba on 02.06.17.
//

#ifndef DRATEWKATHEANNIHILATOR_THREADSAFE_H
#define DRATEWKATHEANNIHILATOR_THREADSAFE_H
#include <pthread.h>

class ThreadSafe {
private:
    pthread_mutex_t mutex;

public:
    ThreadSafe() {mutex = PTHREAD_MUTEX_INITIALIZER;}
    void lock() {pthread_mutex_lock(&mutex);}
    void unlock() {pthread_mutex_unlock(&mutex);}
};


#endif //DRATEWKATHEANNIHILATOR_THREADSAFE_H
