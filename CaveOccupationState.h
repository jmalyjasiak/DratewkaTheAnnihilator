//
// Created by kuba on 03.06.17.
//

#ifndef DRATEWKATHEANNIHILATOR_CAVEOCCUPATIONSTATE_H
#define DRATEWKATHEANNIHILATOR_CAVEOCCUPATIONSTATE_H
#include <string>
using namespace std;
enum CaveOccupationState {
    OWIECZKA_HAS_BEEN_PLANTED,
    BY_DRAGON,
    UNOCCUPIED
};

static string occupationToString(CaveOccupationState state) {
	switch(state) {
	case CaveOccupationState::OWIECZKA_HAS_BEEN_PLANTED :
		return "PODLOZONO OWIECZKE!";
	case CaveOccupationState::BY_DRAGON :
		return "Zajeta przez smoka";
	case CaveOccupationState::UNOCCUPIED :
		return "Pusta";
	default :
		return "Nieznany";
	}
}

#endif //DRATEWKATHEANNIHILATOR_CAVEOCCUPATIONSTATE_H
