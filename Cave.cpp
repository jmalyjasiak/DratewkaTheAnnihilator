//
// Created by kuba on 12.05.17.
//

#include <sstream>
#include "Cave.h"

Cave::Cave(int id) {
    this->id = id;
    this->state = BY_DRAGON;
}

bool Cave::isOccupied() {
    return state != CaveOccupationState::UNOCCUPIED;
}

bool Cave::occupy(CaveOccupationState occupiedBy) {
    if(isOccupied()) {
        return false;
    }
    this->state = occupiedBy;
    return true;
}

void Cave::unoccupy() {
    this -> state = UNOCCUPIED;
}

CaveOccupationState Cave::getOccupiedBy() {
    return this->state;
}

string Cave::getPrintable() {
    ostringstream stream;
     stream << "Id: " << this->id
     	 << " | Stan: " << occupationToString(this->getOccupiedBy());
    return stream.str();
}

int Cave::getId() {
    return this->id;
}

void Cave::setId(int id) {
    this->id = id;
}
