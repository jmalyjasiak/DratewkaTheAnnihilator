//
// Created by kuba on 12.05.17.
//
#include "City.h"
#include <sstream>
City::City(int hp) {
    this->maxHp = this->hp = hp;
}

int City::getHp() {
    return this -> hp;
}

int City::getMaxHp() {
	return this->maxHp;
}

bool City::demolish(int damage) {
    this->hp -= damage;
    return isDemolished();
}

bool City::isDemolished() {
    return (this->hp <= 0);
}

string City::getPrintable() {
    ostringstream stream;
    int hpAsPercent = (this->getHp() / (float) this->getMaxHp()) * 100;
    stream << "HP: ";
    for(auto i = 0; i < hpAsPercent; i += 10) {
        stream << "#";
    }
    stream << " [" << hpAsPercent <<"%] ";
    return stream.str();
}
