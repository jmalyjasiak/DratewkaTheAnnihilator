//
// Created by kuba on 12.05.17.
//

#include <sstream>
#include "Dragon.h"

Dragon::Dragon(int id) : ThreadSafe() {
    this->id = id;
    state = IN_CAVE;
    chased = false;
    cave=nullptr;
    this->seed = rand();
}

bool Dragon::isDead() {
	return this->state == DragonState::DEAD;
}

int Dragon::getId() {
    return this->id;
}

DragonState Dragon::getState() {
    return this->state;
}

void Dragon::setState(DragonState state) {
    this->state = state;
}

int Dragon::attack() {
    return rand() % MAX_DAMAGE;
}

Cave *Dragon::getCave() {
    return this->cave;
}

void Dragon::setCave(Cave *cave) {
    this->cave = cave;
}

bool Dragon::isChased(){
	return this->chased;
}

void Dragon::setChased(bool chased) {
	this->chased = chased;
}

uint * Dragon::getSeed() {
	return &seed;
}

string Dragon::getPrintable() {
    ostringstream stream;
    stream << "Id: " << this->id
    		<< " | Goniony: " << this->chased
			<< " | Stan: " << this->dragonStateToString(this->getState())
			<< " | Postep: ";
    for(auto i = 0; i < this->getProgress(); i += 10) {
        stream << "#";
    }
    return stream.str();
}

string Dragon::dragonStateToString(DragonState state) {
	switch(state) {
		case DragonState::IN_CAVE:
			return "W jaskini";
		case DragonState::FLYING_TO_CITY:
			return "Leci do miasta";
		case DragonState::ATTACKING:
			return "Atakuje";
		case DragonState::FLYING_TO_CAVE:
			return "Wraca do jaskini";
		case DragonState::DEAD:
			return "Martwy";
		default  :
			return "Nieznany";
	}
}
